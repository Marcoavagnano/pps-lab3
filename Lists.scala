package u03

import u02.Modules.Person.{Teacher}
import u02.Modules.isStudent

object Lists extends App :

  // A generic linkedlist
  enum List[E]:
    case Cons(head: E, tail: List[E])
    case Nil()

  // a companion object (i.e., module) for List
  object List:

    def sum(l: List[Int]): Int = l match
      case Cons(h, t) => h + sum(t)
      case _ => 0

    def map[A, B](l: List[A])(mapper: A => B): List[B] = l match
      case Cons(h, t) => Cons(mapper(h), map(t)(mapper))
      case Nil() => Nil()

    def filter[A](l1: List[A])(pred: A => Boolean): List[A] = l1 match
      case Cons(h, t) if pred(h) => Cons(h, filter(t)(pred))
      case Cons(_, t) => filter(t)(pred)
      case Nil() => Nil()

    def drop[A](l: List[A], n: Int): List[A] = l match
      case Cons(h, t) if n > 0 => drop(t, n - 1)
      case _ => l

    def append[A](left: List[A], right: List[A]): List[A] = left match
      case Cons(h, t) => Cons(h, append(t, right))
      case _ => right

    def flatMap[A, B](l: List[A])(f: A => List[B]): List[B] = l match
      case Cons(h, t) => append(f(h), flatMap(t)(f))
      case _ => Nil()


    import u02.Optionals.*
    import Option.*

    def max(l: List[Int]): Option[Int] = l match
      case Cons(h, t) if filter(t)(_ > h) == Nil() => Some(h)
      case Cons(h, t) => max(t)
      case _ => None()
    import u02.Modules.Person

    def getCoursesByTeacher(l: List[Person]): List[String] = l match
      case Cons(h, t) => map(filter(l)(!isStudent(_)))({ case Teacher(n, course) => course; case _ => "" })
      case _ => Nil()

    def foldRight[A, B](l: List[A])(acc: B)(mapper: (A, B) => B): B = l match
      case Cons(h, t) => mapper(h, foldRight(t)(acc)(mapper))
      case _ => acc

    def foldLeft[A, B](l: List[A])(acc: B)(mapper: (B, A) => B): B = l match
      case Cons(h, t) => mapper(foldLeft(t)(acc)(mapper), h)
      case _ => acc

  val l = List.Cons(10, List.Cons(20, List.Cons(30, List.Nil())))



  import List.*


  println(sum(map(filter(l)(_ >= 20))(_ + 1))) // 21+31 = 52
